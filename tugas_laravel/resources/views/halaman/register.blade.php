<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Form</title>
</head>
<body bgcolor="">
    <h1>Buat Account Baru!</h1>
    <h3>Sign Up Form</h3>
    <form action="/welcome" method="POST">
        @csrf
        <label>First Name:</label> <br><br>
        <input type="text" name="nama_awal" id=""><br><br>
        <label>Last Name:</label> <br><br>
        <input type="text" name="nama_akhir" id=""> <br><br>
        <label>Gender:</label> <br><br>
        <input type="radio" name="sex">Male <br>
        <input type="radio" name="sex">Female <br>
        <input type="radio" name="sex">Other <br><br>
        <label>Nationality:</label> <br><br>
        <select name="Nationality"> 
            <option value="Indonesian">Indonesian</option>
            <option value="US">American</option>
            <option value="Other">Others</option>
        </select><br><br>
        <label>Language Spoken:</label> <br><br>
        <input type="checkbox" name="lang">Bahasa Indonesia <br>
        <input type="checkbox" name="lang">English <br>
        <input type="checkbox" name="lang">Other <br><br>
        <label>Bio:</label> <br><br>
        <textarea name="Bio" cols="30" rows="10"></textarea> <br><br>
        <input type="submit" value="Submit">
    </form>
</body>
</html>