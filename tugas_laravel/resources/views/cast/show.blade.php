@extends('layout.master')

@section('title')
    Selamat Datang !
@endsection

@section('title2')
   Detail Cast {{$cast->id}}
@endsection

@section('content')
<h1>Nama Cast : {{$cast->nama}}</h1>
<h1>Umur Cast : {{$cast->umur}}</h1>
<h1>Bio Cast : {{$cast->bio}}</h1>
@endsection