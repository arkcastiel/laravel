<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function index(){
        return view('halaman.register');
    }

    public function welcome(Request $request){     
        $nama1 = $request['nama_awal'];
        $nama2 = $request['nama_akhir'];
        $kelamin = $request->sex;
        $kebangsaan = $request->Nationality;
        $bahasa = $request['lang'];
        $biodata = $request['bio'];
 
        return view('halaman.welcome', compact('nama1','nama2','kelamin','kebangsaan','bahasa','biodata'));
    }
}
